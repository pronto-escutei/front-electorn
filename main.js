// Modules to control application life and create native browser window
const { Menu, app, BrowserWindow } = require('electron')
const { ipcMain } = require('electron')
const path = require('path')

function createWindow() {
    // Create the browser window.
    const mainWindow = new BrowserWindow({
        width: 1200,
        height: 840,
        webPreferences: {
            preload: path.join(__dirname, 'preload.js')
        }
    });
    // mainWindow.webContents.openDevTools();
    mainWindow.loadFile('view/login.html');
    var menu = Menu.buildFromTemplate([{
        label: 'Menu',
        submenu: [{
            label: 'Exit',
            click() { app.quit() },
            accelerator: 'CmdOrCtrl+W'
        }]
    }])
    Menu.setApplicationMenu(menu);
}

app.on('ready', createWindow);

app.on('window-all-closed', function() {
    if (process.platform !== 'darwin') app.quit();
})

app.on('activate', function() {
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
})