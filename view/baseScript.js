// inicializar variaveis
// const gifLoading = `<img src="https://gifimage.net/wp-content/uploads/2018/04/loading-spinner-animated-gif-12.gif" width="40px">`;
const defaulProfile = "https://www.kindpng.com/picc/m/24-248253_user-profile-default-image-png-clipart-png-download.png";

const gifLoading = `
<div class="preloader-wrapper small active">
                                <!-- spinner-blue -->
                                <div class="spinner-layer spinner-blue">
                                    <div class="circle-clipper left">
                                        <div class="circle"></div>
                                    </div>
                                    <div class="gap-patch">
                                        <div class="circle"></div>
                                    </div>
                                    <div class="circle-clipper right">
                                        <div class="circle"></div>
                                    </div>
                                </div>
                                <!-- spinner-red -->
                                <div class="spinner-layer spinner-red">
                                    <div class="circle-clipper left">
                                        <div class="circle"></div>
                                    </div>
                                    <div class="gap-patch">
                                        <div class="circle"></div>
                                    </div>
                                    <div class="circle-clipper right">
                                        <div class="circle"></div>
                                    </div>
                                </div>
                                <!-- spinner-yellow -->
                                <div class="spinner-layer spinner-yellow">
                                    <div class="circle-clipper left">
                                        <div class="circle"></div>
                                    </div>
                                    <div class="gap-patch">
                                        <div class="circle"></div>
                                    </div>
                                    <div class="circle-clipper right">
                                        <div class="circle"></div>
                                    </div>
                                </div>
                                <!-- spinner-greem -->
                                <div class="spinner-layer spinner-green">
                                    <div class="circle-clipper left">
                                        <div class="circle"></div>
                                    </div>
                                    <div class="gap-patch">
                                        <div class="circle"></div>
                                    </div>
                                    <div class="circle-clipper right">
                                        <div class="circle"></div>
                                    </div>
                                </div>
                            </div>
`;

const urlbase = "http://localhost:3000/";
// const urlbase = "https://jsonplaceholder.typicode.com/";

// Math.floor(Math.random() * (max - min)) + min
const idRandom = Math.floor(Math.random() * (1000 - 10)) + 10;

const modalBasico = `
  <div id="modal${idRandom}" class="modal">
    <div class="modal-content">
      <h4> Aviso </h4>
      <p id="textoModal${idRandom}" >  </p>
    </div>
    <div class="modal-footer">
      <a href="#!" id="acaoModal${idRandom}" class="modal-close waves-effect waves-light btn blue"> Ok </a>
    </div>
  </div>
`;

let token = localStorage.getItem('token');
if (!token) token = "";

let esperandoRequisicao = false;

document.write(modalBasico);

function mostrarMensagem(mensagem, callback) {
    $(`#textoModal${idRandom}`).text(mensagem);
    $(`#modal${idRandom}`).modal("open");
    if (callback) {
        let idAcao = `#acaoModal${idRandom}`;
        $(idAcao).unbind('click');
        $(idAcao).click(callback);
    }
}

function mudarPagina(pagina) {
    window.location.href = pagina;
}

function realizarRequisicao(endPoint, tipoRequisicao, body, idElemento, podeMostrarMensagem, functionAfterError, functionAfterSucesso, acao) {
    var cabecalho = new Headers();
    cabecalho.append("authorization", `Bearer ${token}`);
    cabecalho.append('Content-Type', 'application/json');
    var propiedades = {
        method: tipoRequisicao,
        headers: cabecalho,
        mode: 'cors',
        cache: 'default'
    };
    if (tipoRequisicao.toLowerCase() != "get" && body) {
        propiedades.body = JSON.stringify(body);
    }
    var request = new Request(urlbase + endPoint, propiedades);
    if (!esperandoRequisicao) {
        esperandoRequisicao = true;
        acaoRequest(request, idElemento, functionAfterError, podeMostrarMensagem, functionAfterSucesso, acao);
    } else {
        mostrarMensagem("Operação em andamento");
    }
}

function acaoRequest(request, idElemento, functionAfterError, podeMostrarMensagem, functionAfterSucesso, acao) {
    if (idElemento && idElemento[0] != "#") idElemento = "#" + idElemento;
    const elemento = idElemento ? $(idElemento) : null;
    const valorHtml = idElemento ? elemento.html() : null;
    if (elemento) elemento.html(gifLoading);

    if (podeMostrarMensagem) {
        let msgError = `Não foi possivel realizar ${acao}`;
        let msgSucesso = `${acao} realizado com sucesso`;
        fetch(request)
            .then(function(data) {
                zerarRequisicao(elemento, valorHtml);
                if (data.status == 200) mostrarMensagem(msgSucesso, functionAfterSucesso(data));
                else mostrarMensagem(msgError, functionAfterError(data));
            }).catch(function(error) {
                zerarRequisicao(elemento, valorHtml);
                mostrarMensagem(msgError + "<br />" + error.message, functionAfterError);
            });
    } else {
        fetch(request).then(function(data) {
            zerarRequisicao(elemento, valorHtml);
            if (functionAfterSucesso) functionAfterSucesso(data);
        }).catch(function(error) {
            zerarRequisicao(elemento, valorHtml);
            if (functionAfterSucesso) functionAfterError(error);
        });
    }
}

function zerarRequisicao(elemento, valorAntigo) {
    if (elemento) elemento.html(valorAntigo);
    esperandoRequisicao = false;
}

function valorPelaUrl(variavel) {
    let valores = window.location.search.replace("?", "");
    valores = valores.split("=");
    let index = valores.indexOf(variavel);
    if (index != -1) {
        return valores[index + 1]
    } else {
        return "";
    }
}

M.AutoInit();