let postId = valorPelaUrl("id");

$(document).ready(function() {
    carregarDados();
});


function comentar() {
    let comentario = {
        "Postid": postId,
        "Conteudo": $("#txtComentario").val()
    };
    realizarRequisicao("comentario", "post", comentario, "#comentarAcao", true,
        (error) => { zerarComentario(); },
        (dados) => { zerarComentario(); }, "Comentario"
    )
}

function zerarComentario() {
    $("#txtComentario").val("");
    carregarDados();
}

function getTemplatePost(obj) {
    return `
                <div class="col s12 l2">
                    <img class="materialboxed responsive-img" data-caption="Foto do usuário" src="${obj.fotoUsuario ? obj.fotoUsuario : "https://www.kindpng.com/picc/m/24-248253_user-profile-default-image-png-clipart-png-download.png"}">
                </div>
                <div class="col s12 l10">
                    <div class="row" >

                        <div class="col s12 l2">
                            <img class="materialboxed responsive-img" data-caption="Foto do produto" src="${obj.fotoProduto}">
                        </div>

                        <div style="border-bottom: 1px solid black;" class="col s12 l10">
                            <h4> Descrição </h4>
                            <p>
                            ${obj.conteudo}
                            </p>
                            <div class="row" style="background-color: rgb(40,40,40); border-radius: 20px; padding: 20px;" id="comentarioConteudo">

                                <div class="col s12 ">
                                    <div class="row align-items-end" style="padding: 5px; display: flex; flex-wrap: wrap; background-color: rgba(100,40,255,0.5); border-radius: 10px;">

                                        <div class="col s3 l1">
                                            <img style="border-radius: 35%; " src="https://i.pinimg.com/236x/61/50/88/6150888d29a5afd6d32be79ca525574b.jpg " alt=" " class="responsive-img " />
                                        </div>
                                        <div class="col s9 l11 text-primary valign-wrapper">
                                            Texto Cometario
                                        </div>
                                        <div class="divider"></div>
                                    </div>
                                </div>

                            </div>
                            <div class="row ">
                                <div class="col s12 l2 "></div>
                                <div class="input-field col s10 l8 ">
                                    <textarea id="txtComentario" class="materialize-textarea "></textarea>
                                    <label for="txtComentario">Comentário</label>
                                </div>
                                <div class="col s2 ">
                                    <br />
                                    <span onclick="comentar()" id='comentarAcao' class="waves-effect waves-light btn ">Comentar</span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

    `;
}

function getTemplateComentario(obj) {
    return `
        <div class="col s12 " >
            <div class="row align-items-end" style="padding: 5px; display: flex; flex-wrap: wrap; background-color: rgba(100,40,255,0.5); border-radius: 10px;">

                <div class="col s3 l1">
                    <img width='100px' style="border-radius: 35%; " src="${obj.usuario.foto ? obj.usuario.foto : defaulProfile}" alt=" " class="responsive-img " />
                </div>
                <div class="col s9 l11 text-primary valign-wrapper">
                    ${obj.Conteudo}
                </div>
                <div class="divider"></div>
            </div>
        </div>
    `;
}

function carregarDados() {
    carregarDadosPost();
}

function carregarDadosPost() {

    let sucessoPostGet = (data) => {
        if (data.status == 200) {
            data.json().then(function(valor) {
                console.log(valor);
                const arvore = new Arvore();
                for (let i = 0; i < valor.length; i++) {
                    arvore.adicionar({ index: valor[i].id, obj: valor[i] });
                }
                const objs = arvore.pegarRaiz();
                let objeto = objs.buscar(objs, parseInt(postId));
                /*
                let valores = getTemplatePost(valor);
                $("#conteudoPost").html(valores);
                $("#comentarioConteudo").html(gifLoading);
                */
                let valores = getTemplatePost(objeto);
                $("#conteudoPost").html(valores);
                $("#comentarioConteudo").html(gifLoading);

            });
        } else mostrarMensagem("Erro Busca Post");
    }

    // realizarRequisicao("post/listar/" + postId, "get", null, "conteudoPost", false,
    realizarRequisicao("post/listar/", "get", null, "conteudoPost", false,
        (error) => { mostrarMensagem("falha na conexao"); },
        (data) => {
            sucessoPostGet(data);
            carregarDadosComentario();
        }, "Buscar Produtos");
}

function carregarDadosComentario() {

    let sucessoComentarioGet = (data) => {
        if (data.status == 200) {
            data.json().then(function(valor) {
                let valores = ""
                for (let index = 0; index < Object.keys(valor).length; index++) {
                    valores += getTemplateComentario(valor[index]);
                }
                $("#comentarioConteudo").html(valores);
            });
        } else {
            $("#comentarioConteudo").html("<h1 style='color: red'> Error ao carregar <h1>");
            mostrarMensagem("Erro Busca Comentários");
        }
    }

    realizarRequisicao("comentario/" + postId, "get", null, null, false,
        (error) => { mostrarMensagem("falha na conexao"); },
        (data) => {
            sucessoComentarioGet(data);
        }, "Busca de comentários");
}

class Arvore {

    constructor(value = null) {
        this.raiz = value;
    }

    adicionar(value) {

        if (this.raiz == null) {
            this.raiz = new No(value);
        } else {
            this.raiz = this.raiz.adicionar(value);
        }
    }

    pegarRaiz() {
        return this.raiz;
    }

    buscar(index) {
        raiz.buscar(raiz, index);
    }
}

class No {

    buscar(no, index) {
        console.log(no);
        if (no.value == undefined || no.value == null || no.value.index == null) {
            return null;
        } else if (index < no.value.index) {
            return this.buscar(no.left, index);
        } else if (index > no.value.index) {
            return this.buscar(no.right, index);
        } else {
            return no.value.obj;
        }
    }

    constructor(value, left = null, right = null) {
        this.value = value;
        this.left = left;
        this.right = right;
    }

    adicionar(value) {
        if (value.index > this.value.index) {
            if (this.right) this.right = this.right.adicionar(value);
            else this.right = new No(value);
        } else {
            if (this.left) this.left = this.left.adicionar(value);
            else this.left = new No(value);
        }
        const FatorDeBalanco = this.getFatorDeBalanco();

        if (FatorDeBalanco > 1) {
            const rightFatorDeBalanco = this.right.getFatorDeBalanco();
            if (rightFatorDeBalanco > 0) return this.rodarParaEsquerda();
            else return this.direitaParaEsquerda();
        } else if (FatorDeBalanco < -1) {
            const leftFatorDeBalanco = this.left.getFatorDeBalanco();

            if (leftFatorDeBalanco < 0) {
                const result = this.rightRotate();
                return result;
            } else {
                return this.rotacaoEsquerdaParaDireita();
            }
        } else return this;

    }

    getTamanho() {
        const leftLength = this.left ? this.left.getTamanho() : 0;
        const rightLength = this.right ? this.right.getTamanho() : 0;
        return Math.max(leftLength, rightLength) + 1;
    }

    getFatorDeBalanco() {
        const leftLength = this.left ? this.left.getTamanho() : 0;
        const rightLength = this.right ? this.right.getTamanho() : 0;
        return rightLength - leftLength;
    }

    rodarParaEsquerda() {
        const A = this;
        const B = this.right;
        const C = this.right.right;
        A.right = B.left;
        B.left = A;
        return B;
    }

    rightRotate() {
        const A = this;
        const B = this.left;
        const C = this.left.left;
        A.left = B.right;
        B.right = A;
        return B;
    }

    rotacaoEsquerdaParaDireita() {
        const A = this;
        const B = this.left;
        const C = this.left.right;
        this.left = this.left.rodarParaEsquerda();
        return this.rightRotate();
    }

    direitaParaEsquerda() {
        const A = this;
        const B = this.right;
        const C = this.right.left;
        this.right = this.right.rightRotate();
        return this.rodarParaEsquerda();
    }

    rePopular(novoNo) {
        this.value.index = novoNo.value.index;
        this.left = novoNo.left;
        this.right = novoNo.right;
    }
}