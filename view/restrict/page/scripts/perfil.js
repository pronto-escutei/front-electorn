let mudarSenha = false;
let idusuario = 0;

function tooglePassword() {
    mudarSenha = !mudarSenha;
    let idSenha = "#senha";
    $(idSenha).prop("disabled", !mudarSenha);
    if (mudarSenha) {
        $(idSenha).focus();
        $(idSenha).val("");
    }
}

$(document).ready(function() {
    carregarDados();
});

function carregarDados() {
    let sucessoPerfilGet = (data) => {
        if (data.status == 200) {
            data.json().then(function(valor) {
                alterarValores(valor);
            });
        } else mostrarMensagem("Erro Busca perfil");
    }

    realizarRequisicao("usuario/", "get", null, null, false,
        (error) => {
            console.error(error);
            mostrarMensagem("falha na conexao");
        },
        (data) => { sucessoPerfilGet(data); }, "Busca de Usuário");
}

function alterarValores(valor) {
    idUsuario = valor.ID;
    $("#txtEmail").val(valor.EMAIL);
    $("#url").val(valor.FOTO);
    if (valor.FOTO) $('#fotoImg').attr('src', $("#url").val())
    $("#txtNome").val(valor.NOME);
}

function alterarDados() {
    const usuario = {
        "ID": idUsuario,
        "NOME": $("#txtNome").val(),
        "EMAIL": $("#txtEmail").val(),
        "SENHA": mudarSenha ? $("#senha").val() : "",
        "FOTO": $("#url").val()
    };
    realizarRequisicao("usuario/alterar", "put", usuario, "#btnAlterar", true,
        (error) => { console.log({ erro: error }) },
        (dados) => {}, "Alteração de Usuário"
    )
}