let idProduto = 0;

$(document).ready(function() {
    carregarDados();
});

function cadastrarProduto() {
    let produto = {
        "LOJA_ID": pegarLoja(),
        "ID": 0,
        "LINK": $('#txtUrlProduto').val(),
        "NOME": $('#txtNomeProduto').val(),
        "IMAGEM": $('#txtImageProduto').val(),
        "NOMELOJA": ""
    }
    realizarRequisicao("produto", "post", produto, "#btnCadastrarProdutoAcao", true,
        (error) => { zerarProduto(); },
        (dados) => { zerarProduto() }, "Cadastro Produto"
    )
}

function zerarProduto() {
    const inputs = ["txtNomeProduto", "txtUrlProduto", "txtImageProduto"];
    zerarModal(inputs, "#modalCadastroProduto");
}

function zerarLoja() {
    const inputs = ["txtNomeLoja", "urlLoja"];
    zerarModal(inputs, "#modalCadastroLoja");
}

function zerarModal(inputs, idModal) {
    $(idModal).modal("close");
    for (let index = 0; index < inputs.length; index++) $("#" + inputs[index]).val("");
    carregarDados();
}

function cadastrarLoja() {
    let loja = {
        "id": 0,
        "nome": $('#txtNomeLoja').val(),
        "link": $('#urlLoja').val(),
        "imagem": ""
    }
    realizarRequisicao("loja", "post", loja, "#btnCadastrarLojaAcao", true,
        (dados) => { zerarLoja(); },
        (dados) => { zerarLoja(); }, "Cadastro Loja"
    )
}


function pegarLoja() {
    let valores = $("#autocomplete-loja").val().split(" ");
    return valores[valores.length - 1];
}

function selecionarProduto() {
    let valores = $("#autocomplete-produto").val().split(" ");
    idProduto = valores[valores.length - 1];
    $("#modalSelecionar").modal("close");
}

function autoCompleteProduto(dados) {
    $('#autocomplete-produto').autocomplete({
        data: dados,
        minLength: 0,
        maxLength: 15,
        onAutocomplete: function(valor) {
            // acao produto selected
        }
    });
    if (Object.keys(dados)) {
        $('#autocomplete-produto').val(Object.keys(dados)[0]);
    }
}

function autoCompleteLoja(dados) {
    $('#autocomplete-loja').autocomplete({
        data: dados,
        minLength: 0,
        maxLength: 15,
        onAutocomplete: function(valor) {
            // acao loja selected
        }
    });
    if (Object.keys(dados)) {
        $('#autocomplete-loja').val(Object.keys(dados)[0]);
    }
}

function carregarDados() {
    let sucessoProdutoGet = (data) => {
        if (data.status == 200) {
            data.json().then(function(valor) {
                dataProduto = {};
                for (let index = 0; index < Object.keys(valor).length; index++) {
                    dataProduto[` ${valor[index].NOME} - ${valor[index].NOMELOJA} - ${valor[index].ID}`] = valor[index].IMAGEM;
                }
                autoCompleteProduto(dataProduto);
            });
        } else mostrarMensagem("Erro Busca Produto");
    }
    let sucessoLojaGet = (data) => {
        if (data.status == 200) {
            data.json().then(function(valor) {
                dataProduto = {};
                for (let index = 0; index < Object.keys(valor).length; index++) {
                    dataProduto[` ${valor[index].nome} - ${valor[index].id}`] = null;
                }
                autoCompleteLoja(dataProduto);
            });
        } else mostrarMensagem("Erro Busca Produto");
    }

    realizarRequisicao("produto/*", "get", null, null, false,
        (error) => { mostrarMensagem("falha na conexao"); },
        (data) => {
            sucessoProdutoGet(data);
            realizarRequisicao("loja/*", "get", null, null, false,
                (error) => { mostrarMensagem("falha na conexao"); },
                sucessoLojaGet, "Buscar Lojas");
        }, "Buscar Produtos");

}


function postar() {
    if (idProduto == 0) {
        mostrarMensagem("É necessário escolher um produto");
    } else {
        let post = {
            "conteudo": $("#descricao").val(),
            "produto": idProduto
        }
        realizarRequisicao("post", "post", post, "btnPostar", true, (error) => {}, (sucessso) => { mostrarMensagem("Post realizado com sucesso", () => { mudarPagina("../feed.html") }) }, "Post");
    }
}