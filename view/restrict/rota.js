function addAba(element, newPage) {

    var ajax = new XMLHttpRequest();

    // Seta tipo de requisição e URL com os parâmetros
    ajax.open("GET", newPage, true);

    // Envia a requisição
    ajax.send();

    // Cria um evento para receber o retorno.
    ajax.onreadystatechange = function () {
        // Caso o state seja 4 e o http.status for 200, é porque a requisiçõe deu certo.
        if (ajax.readyState == 4 && ajax.status == 200) {
            var data = ajax.responseText;
            document.getElementById(element).innerHTML = data;
            insertAndExecute(element, data);

        }
    }

    function insertAndExecute(id, text) {
        document.getElementById(id).innerHTML = text;
        var scripts = Array.prototype.slice.call(document.getElementById(id).getElementsByTagName("script"));
        for (var i = 0; i < scripts.length; i++) {
            if (scripts[i].src != "") {
                var tag = document.createElement("script");
                tag.src = scripts[i].src;
                document.getElementsByTagName("head")[0].appendChild(tag);
            }
            else {
                eval(scripts[i].innerHTML);
            }
        }
    }

}
