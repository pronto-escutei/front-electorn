const idElemento = "#conteudo";

/* dto exemple
avaliacaoAtual: 0
avaliacaoNegativa: 10
id: 1
avaliacaoPositiva: 10
conteudo: "conteudo post"
fotoProduto: "https://avantare.com.br/wp-content/uploads/2020/04/teste-ab-1140x470.png"
fotoUsuario: "https://avatarfiles.alphacoders.com/200/200998.png"
idUsuario: 1
*/

const erro = function(error) {
    mostrarMensagem("erro: " + error.message);
}
const sucesso = function(data) {
    if (data.status == 200) {
        data.json().then((dados) => {
            let conteudo = "";
            for (let i = 0; i < dados.length; i++) {
                let linha = dados[i];
                conteudo += pegarTemplate(linha);
            }
            $(idElemento).html(conteudo);
            M.AutoInit();
        });
    } else {
        mostrarMensagem("error: " + data.status);
    }
}

function avaliar(status, avaliazacaoAtual) {
    alert("avaliaçao");
}
$(document).ready(function() {
    carregarDados();
});

function pegarTemplate(linha) {
    return `
        <div class="row">
            <div class="col s12 l2">
                <img class="materialboxed responsive-img" data-caption="Foto do Usuário" src="${linha.fotoUsuario ? linha.fotoUsuario : defaulProfile}">
            </div>
            <div class="col s12 l10" style="text-align: left;">

                <div class="row">
                    <div class="col s12 l2">
                        <img class="materialboxed responsive-img" data-caption="Foto do Produto " src="${linha.fotoProduto}">
                    </div>
                    <div style="border-bottom: 1px solid black;" class="col s12 l10">
                        <h4> Descrição</h4>
                        <p>
                            ${linha.conteudo}
                        </p>
                        <div class="right">
                        <!--
                            <i onclick="avaliar(false, ${linha.avaliacaoAtual})" class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="${linha.avaliacaoNegativa}">thumb_down </i>
                            &nbsp;
                            <i onclick="avaliar(true, ${linha.avaliacaoAtual})" class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="${linha.avaliacaoPositiva}">thumb_up</i>
                             
                        -->
                            <i onclick="mudarPagina('page/comentario.html?id=${linha.id}');" class="material-icons right click">comment</i>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    `;
}

function carregarDados() {
    const url = $("#search").val() ? "post/listar/procurar/" + $("#search").val() : "post/listar";
    realizarRequisicao(url, "get", null, idElemento, false, erro, sucesso)
}