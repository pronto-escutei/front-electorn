function realizarCadastro(idElemento) {
    let errorCadastro = (error) => {
        alert("erro cadastro");
    }
    let sucessoCadastro = (data) => {
        if (data.status == 200)
            mostrarMensagem("Cadastro Realizado com sucesso", () => { mudarPagina("login.html") });
        else
            mostrarMensagem("Usuário existente");
    }
    let usuario = preencherUsuario();
    if (usuario)
        realizarRequisicao("autentificacao/cadastro", "post", usuario, idElemento, false, errorCadastro, sucessoCadastro, "Cadastro");
}

function preencherUsuario() {
    return {
        "NOME": $("#txtNome").val(),
        "EMAIL": $("#txtEmail").val(),
        "SENHA": $("#txtSenha").val(),
        "ID_FACEBOOK": ""
    };
}