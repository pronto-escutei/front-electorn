function realizarLogin(idElemento) {
    let errorLogin = (error) => {
        mostrarMensagem("falha na conexao");
        console.error(error);
    }
    let sucessoLogin = (data) => {
        if (data.status == 200) {
            data.json().then(function(valor) {
                localStorage.setItem('token', valor);
                mudarPagina("restrict/feed.html");
            });
        } else mostrarMensagem("Usuário incorreto");
    }
    let objeto = { login: $("#email").val(), senha: $("#senha").val() };
    if (objeto.login && objeto.senha)
        realizarRequisicao("autentificacao/login", "post", objeto, idElemento, false, errorLogin, sucessoLogin, "Login");
    else
        mostrarMensagem("Campos obrigatórios");
}